const isNumeric = n => !!Number(n) || n === "0";

const getSum = (str1, str2) => {
    if (typeof str1 === "string" && typeof str2 === "string") {
        str1 = (str1.length === 0) ? "0" : str1;
        str2 = (str2.length === 0) ? "0" : str2;
    } else return false;

    if (!isNumeric(str1) || !isNumeric(str2)) {
        return false;
    } else {
        let result = '';
        let f = 0;
        if (str1.length > str2.length) {
            let c = str1.length - str2.length
            for (let i = 0; i < c; i++) {
                str2 = "0" + str2;
            }
        } else {
            let c = str2.length - str1.length
            for (let i = 0; i < c; i++) {
                str1 = "0" + str1;
            }
        }

        for (let i = str1.length - 1; i >= 0; i--) {

            if (Number(str1[i]) + Number(str2[i]) + f < 10) {
                result = result + (Number(str1[i]) + Number(str2[i]) + f).toString();
                f = 0;
            } else {

                result = result + (Number(str1[i]) + Number(str2[i]) + f - 10).toString();
                f = 1;
            }
        }
        result = f === 1 ? result + "1" : result;
        return result.split("").reverse().join("");
    }

};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let res = {p: 0, c: 0};
    for (let e of listOfPosts) {
        if (e.author === authorName) {
            res.p++;
        }
        if (e.comments !== undefined) {
            for (let c of e.comments) {
             if( c.author ===authorName )
                 res.c++;
            }
        }
    }
    return "Post:"+res.p+",comments:"+res.c;
};

const tickets = (people) => {
    let cashDesk = {'25': 0, '50': 0, '100': 0};
    for (let e of people) {
        if (e === 25) {
            cashDesk["25"]++;
        } else if (e === 50) {
            if (cashDesk["25"] > 0) {
                cashDesk["25"]--;
                cashDesk["50"]++;
            } else return 'NO';
        } else if (e === 100) {
            if (cashDesk["50"] > 0 && cashDesk["25"] > 0) {
                cashDesk["25"]--;
                cashDesk["50"]--;
                cashDesk["100"]++;
            } else if (cashDesk["25"] > 2) {
                cashDesk["25"] -= 3;
                cashDesk["100"]++;
            } else {
                return 'NO';
            }
        }
    }
    return "YES";
};
module.exports = {getSum, getQuantityPostsByAuthor, tickets};
